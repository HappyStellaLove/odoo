from odoo import models, fields

class ContactCopy(models.Model):
    _inherit = 'res.partner'
    _description = 'Contact Modifications'

    rccm = fields.Char(string="RCCM", required=False)
    b_p = fields.Char(string="B.P", required=False)