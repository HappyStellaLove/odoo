{
    'name': 'Contact Modifications',
    'category': 'Tools',
    'summary': 'Centralize your address book',
    'description': """
        This module gives you a quick view of your contacts directory, accessible from your home page.
        You can track your vendors, customers, and other contacts.
    """,
    'depends': ['base', 'mail'],
    'data': [
        'views/contact.xml',
        'reports/invoice_report_modification.xml',
    ],
    'application': True,
    'license': 'LGPL-3',
}
